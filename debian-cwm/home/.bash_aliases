alias ll='ls -alh'
alias x='exit'
alias wall='exec /usr/bin/feh -z --bg-fill /home/joe/.wallpapers/* & '
alias off='sudo shutdown -P now'
alias reboot='systemctl reboot'
alias reset='systemctl reboot'
alias up='sudo apt update && sudo apt upgrade'
alias remove='sudo apt autoremove'
alias suspend='systemctl suspend'

# Can remove the "?format=x" for full report. x=1 to 4.
alias full-weather='curl wttr.in/Chinchilla'
alias weather='curl wttr.in/Chinchilla?format=4'

alias usboff='udisksctl power-off -b /dev/sdb'
alias fix='xrandr -s 1600x900'

alias lookup='sdcv -en'
alias word='sdcv -en'
alias search='sdcv -en'
alias look='sdcv -en'
alias s='sdcv -en'
